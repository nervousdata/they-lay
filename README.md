## They lay

Bash script (calling ImageMagick) for generating an image collage. Developed for making concrete poems out of segments of sentences (horizontal strips).

One sine function defines the position where a cut is made, starting circa in the middle. The function has AM (Amplitude Modulation) and FM (Frequency Modulation). The user is asked to enter two values (seperated with space-bar) which define how fast these modulations will be. Interesting results for AM between 1 and 100 and FM between 1 and 30000.

Another sine function slightly changes the width of a snippet with every cut.

The new image is composed by following the order of the cutting process; the first snippet produced is put on the left upper angle of the new page and so forth.

Needs [ImageMagick 7](https://imagemagick.org) installed.

### Instructions
Create a project folder. Put the image you want to cut-up into this folder. But the script (.sh) into this folder, too. Make the .sh file executable. Open a Terminal (Command Line Interface), got to folder and run the script with `bash they-lay.sh`. Enter values for AM and FM and enter the filename. Two folders are created, one for the individual cuts and one for the final montage. Go to folder “pieces” to see and open the new file.

### Example

Snippets with max. width of 62 Pixel. Value for AM: 11, for FM: 21000. `sin(5*(3+x*11))*sin(2*3.1416*(3+(x/21000)))+0.9999`

![example_am11_fm21000.png](example_am11_fm21000.png)
